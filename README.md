# mwheel

![](https://img.shields.io/badge/written%20in-Autohotkey-blue)

Allow changing virtual-desktops in Windows 10 by mousewheel on the desktop.

This simulates a common behaviour from XFCE and other desktop environments.

The download package contains both `.exe` binary and `.ahk` source.

This is a proof-of-concept script with known issues, I invite anyone to patch the remaining bugs.

## Known Issues

- Switching desktops is emulated by keystroke, which has some edge-case behaviour e.g. use of Control may change icon size on the desktop
- Nonmatching mousewheel input isn't always passed through to the underlying window


## Download

- [⬇️ mwheel.7z](dist-archive/mwheel.7z) *(438.75 KiB)*
